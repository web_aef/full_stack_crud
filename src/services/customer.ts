import type Customer from "@/types/Customer";
import http from "./axios";
function getCustomer() {
  return http.get("/customers");
}

function saveCustomer(Customer: Customer) {
  return http.post("/customers", Customer);
}

function updateCustomer(id: number, Customer: Customer) {
  return http.patch(`/customers/${id}`, Customer);
}

function deleteCustomer(id: number) {
  return http.delete(`/customers/${id}`);
}

export default { getCustomer, saveCustomer, updateCustomer, deleteCustomer };
