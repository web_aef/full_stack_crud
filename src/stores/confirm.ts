import { ref } from "vue";
import { defineStore } from "pinia";
import { useProductStore } from "./product";

export const useConfirmStore = defineStore("confirm", () => {
  const isShow = ref(false);
  const product = useProductStore();
  const Id = ref<number>();
  function Delete(id: number) {
    if (Id.value !== -1) {
      product.deleteProduct(id);
      Id.value = -1;
      isShow.value = false;
    }
  }
  function Confirm(id: number) {
    isShow.value = true;
    Id.value = id;
  }

  return { isShow, Delete, Confirm, Id };
});
